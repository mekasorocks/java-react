import * as React from 'react';
import { AppContextConsumer } from './App';

interface IProps {}
export const Visitenkarte: React.SFC<IProps> = (props: IProps) => {
    return(
        <AppContextConsumer key="authInfo">
        {appContext => appContext && appContext.authInfo && (
            <div className="visitenkarte">
                <h3>Visitenkarte</h3>
                <p>{appContext.authInfo.username}</p>
                <p>{appContext.authInfo.client}</p>
                <p>{appContext.authInfo.role}</p>
            </div>
        )
        }
        </AppContextConsumer>
    )
}