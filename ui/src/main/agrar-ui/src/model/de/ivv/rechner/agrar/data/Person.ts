/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
import { Address } from './Address';
import { Phone } from './Phone';

export class Person {
    /*private*/ id : number;

    /*private*/ name : string;

    /*private*/ firstName : string;

    /*private*/ birthday : Date;

    /*private*/ address : Address;

    /*private*/ phones : Array<Phone>;

    public getId() : number {
        return this.id;
    }

    public setId(id : number) {
        this.id = id;
    }

    public getName() : string {
        return this.name;
    }

    public setName(name : string) {
        this.name = name;
    }

    public getFirstName() : string {
        return this.firstName;
    }

    public setFirstName(firstName : string) {
        this.firstName = firstName;
    }

    public getBirthday() : Date {
        return this.birthday;
    }

    public setBirthday(birthday : Date) {
        this.birthday = birthday;
    }

    public getAddress() : Address {
        return this.address;
    }

    public setAddress(address : Address) {
        this.address = address;
    }

    public getPhones() : Array<Phone> {
        return this.phones;
    }

    public setPhones(phones : Array<Phone>) {
        this.phones = phones;
    }

    constructor() {
        if(this.id===undefined) this.id = null;
        if(this.name===undefined) this.name = null;
        if(this.firstName===undefined) this.firstName = null;
        if(this.birthday===undefined) this.birthday = null;
        if(this.address===undefined) this.address = null;
        if(this.phones===undefined) this.phones = null;
    }
}
Person["__class"] = "de.ivv.rechner.agrar.data.Person";



