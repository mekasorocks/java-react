/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
export class Address {
    /*private*/ street : string;

    /*private*/ zip : string;

    /*private*/ city : string;

    /*private*/ state : string;

    public getStreet() : string {
        return this.street;
    }

    public setStreet(street : string) {
        this.street = street;
    }

    public getZip() : string {
        return this.zip;
    }

    public setZip(zip : string) {
        this.zip = zip;
    }

    public getCity() : string {
        return this.city;
    }

    public setCity(city : string) {
        this.city = city;
    }

    public getState() : string {
        return this.state;
    }

    public setState(state : string) {
        this.state = state;
    }

    constructor() {
        if(this.street===undefined) this.street = null;
        if(this.zip===undefined) this.zip = null;
        if(this.city===undefined) this.city = null;
        if(this.state===undefined) this.state = null;
    }
}
Address["__class"] = "de.ivv.rechner.agrar.data.Address";



