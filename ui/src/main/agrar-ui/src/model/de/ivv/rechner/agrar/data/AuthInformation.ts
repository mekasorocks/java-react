/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
export class AuthInformation {
    /*private*/ username : string;

    /*private*/ role : AuthInformation.Role;

    /*private*/ client : AuthInformation.Client;

    /*private*/ userId : number;

    public getUsername() : string {
        return this.username;
    }

    public setUsername(username : string) {
        this.username = username;
    }

    public getRole() : AuthInformation.Role {
        return this.role;
    }

    public setRole(role : AuthInformation.Role) {
        this.role = role;
    }

    public getClient() : AuthInformation.Client {
        return this.client;
    }

    public setClient(client : AuthInformation.Client) {
        this.client = client;
    }

    public getUserId() : number {
        return this.userId;
    }

    public setUserId(userId : number) {
        this.userId = userId;
    }

    constructor() {
        if(this.username===undefined) this.username = null;
        if(this.role===undefined) this.role = null;
        if(this.client===undefined) this.client = null;
        if(this.userId===undefined) this.userId = null;
    }
}
AuthInformation["__class"] = "de.ivv.rechner.agrar.data.AuthInformation";


export namespace AuthInformation {

    export enum Role {
        VP, EBI, FI
    }

    export enum Client {
        VGH, OEVO
    }
}



