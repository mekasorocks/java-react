/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
export class LoginInformation {
    /*private*/ username : string;

    /*private*/ password : string;

    public getUsername() : string {
        return this.username;
    }

    public setUsername(username : string) {
        this.username = username;
    }

    public getPassword() : string {
        return this.password;
    }

    public setPassword(password : string) {
        this.password = password;
    }

    constructor() {
        if(this.username===undefined) this.username = null;
        if(this.password===undefined) this.password = null;
    }
}
LoginInformation["__class"] = "de.ivv.rechner.agrar.data.LoginInformation";



