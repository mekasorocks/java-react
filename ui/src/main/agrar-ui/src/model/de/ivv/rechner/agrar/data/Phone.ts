/* Generated from Java with JSweet 2.1.0-SNAPSHOT - http://www.jsweet.org */
import { PhoneType } from './PhoneType';

export class Phone {
    /*private*/ phonetype : PhoneType;

    /*private*/ number : string;

    public getPhonetype() : PhoneType {
        return this.phonetype;
    }

    public setPhonetype(phonetype : PhoneType) {
        this.phonetype = phonetype;
    }

    public getNumber() : string {
        return this.number;
    }

    public setNumber(number : string) {
        this.number = number;
    }

    constructor() {
        if(this.phonetype===undefined) this.phonetype = null;
        if(this.number===undefined) this.number = null;
    }
}
Phone["__class"] = "de.ivv.rechner.agrar.data.Phone";



