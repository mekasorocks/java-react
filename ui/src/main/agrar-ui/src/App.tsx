import axios from 'axios';
import * as React from 'react';
import './App.css';
import { Person } from './model/de/ivv/rechner/agrar/data/Person';
import { Address } from './model/de/ivv/rechner/agrar/data/Address';
import { LoginInformation } from './model/de/ivv/rechner/agrar/data/LoginInformation';
import { AuthInformation } from './model/de/ivv/rechner/agrar/data/AuthInformation';
import { Visitenkarte } from './Visitenkarte';


interface IAppProps {}
interface IAppState {
  authenticated: boolean;
  personen: Array<Person>;
  adresse: Address;
}
export interface IAppContext {
  authInfo: AuthInformation;
}

const appContext = React.createContext<IAppContext | null>(null);
export const AppContextProvider = appContext.Provider;
export const AppContextConsumer = appContext.Consumer;

class App extends React.Component<IAppProps, IAppState> {
  appContext: IAppContext;

  constructor(props: IAppProps, state: IAppState) {
    super(props, state);
    this.state = {
      authenticated: false,
      personen: [],
      adresse: null
    }
    this.appContext = {
      authInfo: null
    }
    this.authenticate();
  }

  /**
   * @description Holt die Personen aus dem Backend und schreibt sie in den State.
   */
  public componentDidMount() {
    axios.get<Array<Person>>("http://localhost/api/person")
    .then(response => {
      const empfangenePersonen: Array<Person> = [];
      console.log("Personen Response", response.data);
      response.data.map(person => {
        empfangenePersonen.push(person);
      })
      this.setState({
        personen: empfangenePersonen
      });
    }).catch(error => {
      console.log("Personen laden error", error);
    })
  }

  /**
   * @description Authentifiziert einen Benutzer gegen das Backend. Die LoginInformationen werden über AppContextProvider
   * an die registrierten AppContextConsumer weitergeleitet.
   */
  authenticate = () => {
    const login: LoginInformation = new LoginInformation();
    login.setUsername("Mayer");
    login.setPassword("Hallo123");
    console.log("Sending", login);
    axios.post<AuthInformation>("http://localhost/api/person/auth", login)
    .then(response => {
      this.appContext.authInfo = response.data;
      console.log(this.appContext.authInfo);
      this.setState({
        authenticated: true
      });
    })
    .catch(error => {
      console.log("Auth error", error);
      this.setState({
        authenticated: false
      });
    });
  } 

  /**
   * @description Nach der Auswahl einer Person wird die Adresse vom Backend geholt und in den State geschrieben.
   * @param {number} personId die ID der ausgewählten Person  
   */
  selectPersonHandler = (personId: number) => {
    console.log("Ausgewählte Person", personId);
    axios.get<Address>("http://localhost/api/person/" + personId + "/address")
    .then(response => {
      const empfangeneAdresse: Address = response.data;
      console.log("Empfangene Adresse", empfangeneAdresse);
      this.setState({
        adresse: empfangeneAdresse
      })
    })
    .catch(error => {
      console.log("Error", error);
    })
  }

  public render() {
    const personenAnzeige = (
      <div>
        {
          this.state.personen.map(person => {
            return (
              <div key={person.id}>
                <input 
                  type="radio" 
                  name="person" 
                  onChange={this.selectPersonHandler.bind(this, person.id)} />{person.firstName} {person.name}
              </div>
            )
          })
        }
      </div>
    )

    let addressenAnzeige = null;
    if (this.state.adresse !== null) {
      addressenAnzeige = (
        <div>
          <p>{this.state.adresse.street}</p>
          <p>{this.state.adresse.city} {this.state.adresse.zip}</p>
          <p>{this.state.adresse.state}</p>
        </div>
      )
    }

    return (
      <AppContextProvider value={this.appContext}>
        {personenAnzeige} 
        {addressenAnzeige}
        <Visitenkarte />
      </AppContextProvider>
    );
  }
}

export default App;
