package de.ivv.rechner.agrar.web;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import de.ivv.rechner.agrar.data.LoginInformation;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;


public class Webserver extends AbstractVerticle {
	private static Logger LOGGER = LoggerFactory.getLogger(Webserver.class);
	private static final String CONTENT_TYPE = "application/json";
	private static final int DEFAULT_PORT_NO = 80;
	
	private HttpServer server;
	
	private PersonServiceImpl personService;

	/**
	 * Just to start the webserver within an IDE.
	 * @param args command line args
	 */
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(Webserver.class.getName());
	}

	public void start(Future<Void> startFuture) throws Exception {
		LOGGER.info("starting...");
		this.server = this.vertx.createHttpServer();
		Router router = Router.router(vertx);

		// allow clients to send all kinds of requests
		Set<String> allowedHeaders = new HashSet<>();
	    allowedHeaders.add(HttpHeaders.CONTENT_TYPE.toString());
	    allowedHeaders.add(HttpHeaders.ACCEPT.toString());
		router.route().handler(CorsHandler.create("*")
				.allowedMethods(new HashSet<HttpMethod>(Arrays.asList(HttpMethod.values())))
				.allowedHeaders(allowedHeaders));
		
		// rest service methods
		// read all persons
		router.get("/api/person").produces(CONTENT_TYPE).handler(this::addRouteGetAllPersons);
		// read one address
		router.get("/api/person/:personId/address").produces(CONTENT_TYPE).handler(this::addRouteGetAddressByPersonId);
		// handle all requests that have a request body
		router.route().handler(BodyHandler.create());
		// authentication
		router.post("/api/person/auth").consumes(CONTENT_TYPE).produces(CONTENT_TYPE).handler(this::addRouteForAuthentication);
		// serve static web resources (UI files, html, js...)
		router.getWithRegex("^(?!.*\\\\bapi\\\\b).*").handler(StaticHandler.create().setCachingEnabled(false));
		
		int portNo = this.config().getInteger("server.port", DEFAULT_PORT_NO);
		server.requestHandler(router::accept).listen(portNo, asyncResult -> {
			if (asyncResult.succeeded()) {
				this.personService = new PersonServiceImpl();
				registerJsonModules();
				LOGGER.info("server is started on port {0}", portNo);
				startFuture.complete();
			} else {
				LOGGER.error("server could not be startet on port {0}", portNo, asyncResult.cause());
				startFuture.fail(asyncResult.cause());
			}
		});
	}
	
	private void registerJsonModules() {
		// special module for Java8 date time classes
		JavaTimeModule module = new JavaTimeModule();
		module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ISO_DATE_TIME));
		Json.mapper.registerModule(module).setTimeZone(TimeZone.getTimeZone("UTC"));
		Json.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		Json.prettyMapper.registerModule(module).setTimeZone(TimeZone.getTimeZone("UTC"));
		Json.prettyMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}
	
	private void addRouteGetAllPersons(RoutingContext routingContext) {
		routingContext.response()
	      .end(Json.encodePrettily(this.personService.getAll()));
	}
	
	private void addRouteGetAddressByPersonId(RoutingContext routingContext) {
		String param = routingContext.request().getParam("personId");
		Integer personId = Integer.parseInt(param);
		routingContext.response()
	      .end(Json.encodePrettily(this.personService.findAddressByPersonId(personId)));
	}
	
	private void addRouteForAuthentication(RoutingContext routingContext) {
		JsonObject json = routingContext.getBodyAsJson();
		LOGGER.info("JSON:" + json);
		if (json != null) {
			LoginInformation loginInfo = Json.decodeValue(json.encode(), LoginInformation.class);
			LOGGER.info("Login: " + loginInfo.getUsername() + " " + loginInfo.getPassword());
			routingContext.response()
		      .end(Json.encodePrettily(this.personService.authenticate(loginInfo)));
		} else {
			routingContext.response()
		      .end(Json.encodePrettily("Bad request"));
		}
	}

	public void stop(Future<Void> stopFuture) {
		stopFuture.complete();
	}
}
