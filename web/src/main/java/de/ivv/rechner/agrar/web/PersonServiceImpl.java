package de.ivv.rechner.agrar.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.ivv.rechner.agrar.data.Address;
import de.ivv.rechner.agrar.data.AuthInformation;
import de.ivv.rechner.agrar.data.AuthInformation.Client;
import de.ivv.rechner.agrar.data.AuthInformation.Role;
import de.ivv.rechner.agrar.data.LoginInformation;
import de.ivv.rechner.agrar.data.Person;
import de.ivv.rechner.agrar.data.Phone;
import de.ivv.rechner.agrar.data.PhoneType;

public class PersonServiceImpl {

	private List<Person> allPersons;
	private Map<Integer, Address> adressen;

	public PersonServiceImpl() {
		this.allPersons = new ArrayList<>();
		this.adressen = new HashMap<>();
		Person person = new Person();
		person.setId(1);
		person.setName("Simpson");
		person.setFirstName("Josh");
		person.setBirthday(new Date());

		Phone phone = new Phone();
		phone.setNumber("02677 - 92788000");
		phone.setPhonetype(PhoneType.Landline);

		person.setPhones(Arrays.asList(phone));
		allPersons.add(person);
		person = new Person();
		person.setId(2);
		person.setName("Mayer");
		person.setFirstName("Gwen");
		person.setBirthday(new Date());
		phone = new Phone();
		phone.setNumber("02691 - 12345000");
		phone.setPhonetype(PhoneType.Mobile);
		person.setPhones(Arrays.asList(phone));
		allPersons.add(person);
		this.adressen = new HashMap<>();
		Address address = new Address();
		address.setCity("Birmingham");
		address.setZip("82788");
		address.setState("New York");
		address.setStreet("Main Street 92");

		this.adressen.put(1, address);
		address = new Address();
		address.setCity("Nottingham");
		address.setZip("12638");
		address.setState("California");
		address.setStreet("Center Station 6");
		this.adressen.put(2, address);
	}

	public List<Person> getAll() {
		return this.allPersons;
	}

	public Address findAddressByPersonId(Integer personID) {
		return this.adressen.get(personID);
	}
	
	public AuthInformation authenticate(LoginInformation login) {
		AuthInformation authInfo = null;
		Optional<Person> first = this.allPersons.stream().filter(person -> person.getName().equals(login.getUsername())).findFirst();
		if (first.isPresent()) {
			authInfo = new AuthInformation();
			authInfo.setUserId(first.get().getId());
			authInfo.setClient(Client.VGH);
			authInfo.setRole(Role.VP);
			authInfo.setUsername(first.get().getFirstName() + " " + first.get().getName());
		}
		return authInfo;
	}
}
