package de.ivv.rechner.agrar.data;

public class Phone {

	private PhoneType phonetype;
	private String number;
	public PhoneType getPhonetype() {
		return phonetype;
	}
	public void setPhonetype(PhoneType phonetype) {
		this.phonetype = phonetype;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
