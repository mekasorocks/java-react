package de.ivv.rechner.agrar.data;

public class AuthInformation {
	public enum Role {
		VP, EBI, FI
	}
	public enum Client {
		VGH, OEVO
	}
	private String username;
	private Role role;
	private Client client;
	private Integer userId;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
